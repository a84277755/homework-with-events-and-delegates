﻿namespace Documents.Interfaces
{
    interface IDocumentsReceiver
    {
        /// <summary>
        /// This method will start listening for new files
        /// </summary>
        /// <param name="path">absolute path to required folder</param>
        /// <param name="timeout">time in ms</param>
        void Start(string path, int timeout);
    }
}
