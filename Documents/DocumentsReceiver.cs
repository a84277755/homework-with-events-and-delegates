﻿using Documents.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;

namespace Documents
{
    public delegate void DocumentsReadyHandler(object sender, EventArgs e);
    public delegate void TimedOutHandler(object sender, EventArgs e);

    public delegate void CorrectDocumentUploadedHandler(object sender, string documentName, int restCount);
    public delegate void CorrectDocumentRemovedHandler(object sender, string documentName, int restCount);

    public class DocumentsReceiver : IDocumentsReceiver
    {
        public event DocumentsReadyHandler DocumentsReady;
        public event TimedOutHandler TimedOut;

        public event CorrectDocumentUploadedHandler CorrectDocumentUploaded;
        public event CorrectDocumentRemovedHandler CorrectDocumentRemoved;

        private FileSystemWatcher fileSystemWatcher;
        private Timer timer;
        private List<string> uploadedDocuments = new List<string>();

        private bool started;

        public void Start(string requiredPath, int timeout)
        {
            if (started)
            {
                throw new Exception("Already started");
            }

            fileSystemWatcher = new FileSystemWatcher(requiredPath);
            fileSystemWatcher.NotifyFilter = NotifyFilters.FileName;
            fileSystemWatcher.EnableRaisingEvents = true;

            fileSystemWatcher.Created += OnCreate;
            fileSystemWatcher.Deleted += OnDelete;

            this.TimedOut += RemoveListeners;
            this.DocumentsReady += RemoveListeners;

            timer = new Timer(timeout);
            timer.Elapsed += OnElapsed;
            timer.Start();

            started = true;
        }

        private void RemoveListeners(object sender, EventArgs e)
        {
            fileSystemWatcher.Created -= OnCreate;
            fileSystemWatcher.Deleted -= OnDelete;
            timer.Elapsed -= OnElapsed;

            this.TimedOut -= RemoveListeners;
            this.DocumentsReady -= RemoveListeners;

            timer.Dispose();
            fileSystemWatcher.Dispose();

            timer.Stop();
        }

        private void OnElapsed (object sender, ElapsedEventArgs e)
        {
            TimedOut.Invoke(this, e);
        }

        private void OnCreate(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Created)
            {
                if (DocumentConstants.allowedFileNames.Contains(e.Name) && !uploadedDocuments.Contains(e.Name))
                {
                    uploadedDocuments.Add(e.Name);
                    CorrectDocumentUploaded.Invoke(this, e.Name, DocumentConstants.allowedFileNames.Count - uploadedDocuments.Count);

                    if (uploadedDocuments.Count == DocumentConstants.allowedFileNames.Count)
                    {
                        DocumentsReady.Invoke(this, e);
                    }
                }
            }
        }

        private void OnDelete(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Deleted)
            {
                if (DocumentConstants.allowedFileNames.Contains(e.Name) && uploadedDocuments.Contains(e.Name))
                {
                    uploadedDocuments.Remove(e.Name);
                    CorrectDocumentRemoved.Invoke(this, e.Name, DocumentConstants.allowedFileNames.Count - uploadedDocuments.Count);
                }
            }
        }
    }
}
