﻿using System.Collections.Generic;

namespace Documents
{
    class DocumentConstants
    {
        public static List<string> allowedFileNames = new List<string>(){
            "Паспорт.jpg",
            "Заявление.txt",
            "Фото.jpg"
        };
    }
}
