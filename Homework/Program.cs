﻿using System;
using System.IO;
using Documents;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            string fullPathToFolder = Path.GetFullPath(Path.Combine("/", "developing", "targetDirectory"));

            DocumentsReceiver documentsReceiver = new DocumentsReceiver();
            documentsReceiver.Start(fullPathToFolder, 60000);

            documentsReceiver.DocumentsReady += OnDocumentsReady;
            documentsReceiver.TimedOut += OnTimedOut;
            documentsReceiver.CorrectDocumentUploaded += OnDocumentUploaded;
            documentsReceiver.CorrectDocumentRemoved += OnDocumentRemoved;

            Console.ReadLine();
        }

        private static void OnDocumentsReady (object sender, EventArgs e)
        {
            Console.WriteLine("All documents are uploaded");
        }

        private static void OnTimedOut(object sender, EventArgs e)
        {
            Console.WriteLine("Timed out!");
        }

        private static void OnDocumentUploaded (object sender, string documentName, int restCount)
        {
            Console.WriteLine($"Uploaded document: {documentName}, rest count: {restCount}");
        }

        private static void OnDocumentRemoved(object sender, string documentName, int restCount)
        {
            Console.WriteLine($"Removed document: {documentName}, rest count: {restCount}");
        }
    }
}
